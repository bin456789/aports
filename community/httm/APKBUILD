# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=httm
pkgver=0.33.5
pkgrel=0
pkgdesc="Interactive, file-level Time Machine-like tool for ZFS/btrfs"
url="https://github.com/kimono-koans/httm"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="MPL-2.0"
makedepends="cargo acl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="https://github.com/kimono-koans/httm/archive/refs/tags/$pkgver/httm-$pkgver.tar.gz"
options="net !check"  # no tests provided

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features acls
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
	install -D -m644 $pkgname.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
33fc85aec6d9134742ea91a0d8ac6a63e4404dda9b772230fc05fdcdfc25c5c43c1755efa1c6534ec6f36e5aa6ebc7603968db3de79b662132d3d1f41a55ba62  httm-0.33.5.tar.gz
"
