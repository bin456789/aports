# Contributor: Adam Jensen <adam@acj.sh>
# Maintainer: Adam Jensen <adam@acj.sh>
pkgname=rbspy
pkgver=0.18.0
pkgrel=0
pkgdesc="Sampling profiler for Ruby"
url="https://rbspy.github.io/"
arch="all !armv7 !ppc64le !s390x !riscv64" # limited by cargo and build errors
license="MIT"
makedepends="cargo cargo-auditable"
checkdepends="ruby"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/rbspy/rbspy/archive/v$pkgver.tar.gz"


build() {
	cargo auditable build --release --locked
}

check() {
	# Some tests need additional privileges
	cargo test --release --locked -- \
		--skip sampler::tests \
		--skip test_current_thread_address \
		--skip test_initialize_with_disallowed_process \
		--skip test_get_exec_trace \
		--skip test_get_trace
}

package() {
	install -Dm755 "target/release/rbspy" "$pkgdir/usr/bin/rbspy"
}

sha512sums="
4829414d2dbf2ce48583476ff7c4f4d8009279a32d1cf2423c5aad7d0ab3ca6140d8f4790e8d22b54761b54592c47d293cb99262b44e448c7c5fd77449276b9c  rbspy-0.18.0.tar.gz
"
